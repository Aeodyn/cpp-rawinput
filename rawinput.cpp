// Adapted from https://gist.github.com/luluco250/ac79d72a734295f167851ffdb36d77ee
// cl /O2 /EHsc /c /MD /std:c++latest rawinput.cpp && link rawinput user32.lib


#define NOMINMAX
#define VC_EXTRALEAN
#include <Windows.h>
#include <intrin.h>
#include <sysinfoapi.h>
#include <timezoneapi.h>
#include <fstream>
#include <string>

#pragma intrinsic(__rdtscp)


// Dummy variable
unsigned int _;
unsigned int* __ = &_;

std::string ofpath = "rawinput.csv";
unsigned int hotkey = VK_SPACE;
bool busywait = false;
bool capture = true;
unsigned int cpumask = 0;


FILETIME startQ;
FILETIME endQ;
unsigned long long startT;
unsigned long long endT;

const size_t NEVENTS = 8000 * 30;
unsigned long long tscs[NEVENTS];
int xs[NEVENTS];
int ys[NEVENTS];
size_t i = 0;
int stage = 0;

LRESULT CALLBACK EventHandler(HWND, unsigned, WPARAM, LPARAM);


void parseArgs() {
	for (int k = 0; k < __argc; k++) {
		if (__argv[k][0] != '-')
			continue;
		switch (__argv[k][1]) {
		case 'p':
			ofpath = __argv[++k];
			break;
		case 'k':
			hotkey = std::stoi(__argv[++k], nullptr, 0);
			break;
		case 'b':
			busywait = true;
			break;
		case 'n':
			capture = false;
			break;
		case 'c':
			cpumask = 1<<std::stoi(__argv[++k], nullptr, 0);
			break;
		}
	}
}

void end() {
	GetSystemTimePreciseAsFileTime(&endQ);
	endT = __rdtscp(__);

	ULARGE_INTEGER sQ{};
	ULARGE_INTEGER eQ{};
	sQ.LowPart = startQ.dwLowDateTime; sQ.HighPart = startQ.dwHighDateTime;
	eQ.LowPart = endQ.dwLowDateTime; eQ.HighPart = endQ.dwHighDateTime;
	double ratio = (eQ.QuadPart - sQ.QuadPart) / (double)(endT - startT);

	std::wofstream outfile(ofpath);
	outfile << L"k\ttsc\tx\ty\tdate\ttime\tns\trel\tdt\n";
	for (size_t k = 0; k < i; k++) {
		lldiv_t t = lldiv((tscs[k] - startT) * (eQ.QuadPart - sQ.QuadPart), endT - startT);
		unsigned long long timeI = sQ.QuadPart + t.quot;
		double timeF = (t.rem * 100) / (double)(endT - startT);

		FILETIME localtime;
		FileTimeToLocalFileTime((FILETIME*)&timeI, &localtime);
		SYSTEMTIME systime;
		FileTimeToSystemTime(&localtime, &systime);
		wchar_t dateStr[255];
		wchar_t timeStr[255];
		GetDateFormatEx(LOCALE_NAME_USER_DEFAULT, 0, &systime, NULL, dateStr, 255, NULL);
		GetTimeFormatEx(LOCALE_NAME_USER_DEFAULT, TIME_FORCE24HOURFORMAT, &systime, NULL, timeStr, 255);
		unsigned long ns = (timeI % 10000000) * 100 + timeF + 0.5;
		double trel = t.quot*100 + timeF;

		outfile << k << '\t';
		outfile << tscs[k] << '\t';
		outfile << xs[k] << '\t';
		outfile << ys[k] << '\t';
		outfile << dateStr << '\t';
		outfile << timeStr << '\t';
		outfile << ns << '\t';
		outfile << std::format(L"{}", trel) << '\t';
		if (k > 0)
			outfile << std::format(L"{}", ((tscs[k] - tscs[k-1]) * 100) * ratio);
		outfile << '\n';
	}
	outfile.close();
}

int APIENTRY WinMain(HINSTANCE hInst, HINSTANCE hInstPrev, PSTR cmdline, int cmdshow) {
	parseArgs();

	// Lock to either current proc or the specifed one.
	if (cpumask == 0)
		cpumask = 1<<GetCurrentProcessorNumber();
	SetThreadAffinityMask(GetCurrentThread(), cpumask);  // SetProcessAffinityMask would have slight issues if MaxLoaderThreads isn't set to 1.
	SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);

	HINSTANCE instance = GetModuleHandle(0);

	RegisterHotKey(NULL, 1, MOD_NOREPEAT, hotkey);

	// Window has to be focusable else DWM throttles its GRID calls.
	const char* class_name = "RawInput Class";

	WNDCLASS window_class = {};
	window_class.lpfnWndProc = EventHandler;
	window_class.hInstance = instance;
	window_class.lpszClassName = class_name;

	if (!RegisterClass(&window_class))
		return -1;

	HWND window = CreateWindow(class_name, "RawInput", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 10, 10, NULL, NULL, instance, NULL);

	if (window == nullptr)
		return -1;
	
	ShowWindow(window, cmdshow);

	// Registering raw input devices
	#ifndef HID_USAGE_PAGE_GENERIC
	#define HID_USAGE_PAGE_GENERIC ((unsigned short) 0x01)
	#endif
	#ifndef HID_USAGE_GENERIC_MOUSE
	#define HID_USAGE_GENERIC_MOUSE ((unsigned short) 0x02)
	#endif

	RAWINPUTDEVICE rid[1];
	rid[0].usUsagePage = HID_USAGE_PAGE_GENERIC;
	rid[0].usUsage = HID_USAGE_GENERIC_MOUSE;
	if (capture)
		rid[0].dwFlags = RIDEV_INPUTSINK | RIDEV_NOLEGACY | RIDEV_CAPTUREMOUSE;
	else
		rid[0].dwFlags = RIDEV_INPUTSINK | RIDEV_NOLEGACY;
	rid[0].hwndTarget = window;
	RegisterRawInputDevices(rid, 1, sizeof(rid[0]));

	MSG event;

	// Record start and end times to calibrate TSC timestamps to wall time.
	GetSystemTimePreciseAsFileTime(&startQ);
	startT = __rdtscp(__);

	// GetMessage waits for Windows to alert; PeekMessage is busy-wait.
	if (busywait) {
		while (stage < 2) {
			while (PeekMessage(&event, 0, 0, 0, PM_REMOVE)) {
				if (event.message == WM_HOTKEY) {
					stage++;
					break;
				}
				DispatchMessage(&event);
			}
		}
	} else {
		while (stage < 2) {
			while (GetMessage(&event, 0, 0, 0)) {
				if (event.message == WM_HOTKEY) {
					stage++;
					break;
				}
				DispatchMessage(&event);
			}
		}
	}
	end();
	return 0;
}

LRESULT CALLBACK EventHandler(
	HWND hwnd,
	unsigned event,
	WPARAM wparam,
	LPARAM lparam
) {
	static unsigned long long tsc;
	tsc = __rdtscp(__);
	switch (event) {
		case WM_DESTROY: {
			PostQuitMessage(0);
			return 0;
		}
		case WM_INPUT: {
			static unsigned size = sizeof(RAWINPUT);
			static RAWINPUT raw[sizeof(RAWINPUT)];
			GetRawInputData((HRAWINPUT)lparam, RID_INPUT, raw, &size, sizeof(RAWINPUTHEADER));

			if (stage == 1 && raw->header.dwType == RIM_TYPEMOUSE) {
				tscs[i] = tsc;
				xs[i] = raw->data.mouse.lLastX;
				ys[i] = raw->data.mouse.lLastY;
				if (i++ >= NEVENTS)
					stage = 2;
			}
		}
		return 0;
	}

	// Run default message processor for any missed events:
	return DefWindowProc(hwnd, event, wparam, lparam);
}
